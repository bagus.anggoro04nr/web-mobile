# web-mobile

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.0.
Saya tidak sertakan CSS-nya or Stylesheet...

Fitur :

- This project use proxy-config multi port.
- Dynamic template menu and button

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Web Mobile
![ScreenShot](Screenshot_2020-09-26-10-54-41-937_com.android.chrome.png)

## Video
https://youtu.be/ELzfeYT7QiY

## Full Video
https://drive.google.com/file/d/1n7_K0mpMRVa6f8kkGhzsp4Px3Kqb4O9f/view?usp=sharing

## Thanks
Google inc, StackOverflow,  and anymore yang tidak bisa ku sebut.. 
karena ada yang tidak senang saya bila menyebut forum atau vendor... 
thanks Indonesia..
