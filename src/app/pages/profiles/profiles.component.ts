import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { UploadService } from '../../_config/_services/upload.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {
  uploadForm: FormGroup;
  editForm: FormGroup;
  returnUrl: string;
  loading = false;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  allowed_types: ['application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'image/jpg',
  'image/jpeg',
  'application/pdf',
  'image/png',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  dataUpload: any;
  dataEdit: any;
  error = '';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private uploadService: UploadService) { this.loading = true; }

  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      photoFile: ['']
    });

    this.editForm = this.formBuilder.group({
      namaLengkap: [''],
      email: [''],
      tglLahir: [''],
      Alamat: ['']

    });
    this.loading = true;
  }

  get u() { return this.uploadForm.controls; }
  get e() { return this.editForm.controls; }

  onUpload() {
    this.dataUpload = {'dataPhoto': this.toBase64(this.u.photoFile.value), 'type': 'photo'};
    this.uploadService.upload(this.dataUpload)
        .then( (data) =>{
          console.log(data);

        }, (error) => {
          console.log(error);
          this.error = error;
        });
  }

  onEdit() {}

  toHome() {
    this.returnUrl = 'home';
    this.router.navigate(['home']);
  }

  toBase64(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/png', 'image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
            'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }

      if (!_.includes(this.allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( JPG | PNG )';
        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          console.log(img_height, img_width);


          if (img_height > max_height && img_width > max_width) {
            this.imageError =
                'Maximum dimentions allowed ' +
                max_height +
                '*' +
                max_width +
                'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
            // this.previewImagePath = imgBase64Path;
          }
        };
      };

      return reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  removeImage() {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
  }
}
