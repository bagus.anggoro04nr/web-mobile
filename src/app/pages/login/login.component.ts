/* tslint:disable */
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../_config/_services/authentication.service';
import {TransferData} from '@src/app/_config/_libs/transfer.data';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    returnUrl: string;
    error = '';
    dataLogin: any;
    userLogin: any;
    userMenu: any;
    navBar: any;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        public router: Router,
        private authenticationService: AuthenticationService,
        public transferData: TransferData,
    ) {
           if (this.authenticationService.currentUserValue) {
                this.router.navigate(['']);
            }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]

        });
         this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalids
        if (this.loginForm.invalid) {
            return;
        }


        this.dataLogin = {'email': this.f.email.value, 'pass': this.f.password.value};
        this.authenticationService.login(this.dataLogin)
       .then( (user) =>{

            this.userLogin = user;
          if(this.userLogin != null) {

               this.menu();
               this.transferData.dataLogin(JSON.stringify(user));
              this.returnUrl = 'home';
              this.router.navigate(['navbar']);
              this.router.navigate( ['home']);
          }

            return this.userLogin;
        }, (error) => {
            
            console.log(error);
            this.error = error ;

        });



    }

    menu() {
        this.authenticationService.menu(this.dataLogin)
            .then((menu) => {

                this.userMenu = menu;
                // console.log(this.userMenu);


                if(this.userMenu !=null){
                    this.transferData.dataMenu(JSON.stringify(menu));

                    this.navbar();
                }
                return this.userMenu;
            }, (error) => {

                console.log(error);
                this.error = error;

            });
    }

    navbar() {
        // this.authenticationService.menunavbar(this.dataLogin)
        //     .then( (menuBar) =>{
        //
        //         this.navBar = menuBar;
        //         console.log(this.userLogin);
        //
        //         this.router.navigate([ '',], {state: this.navBar });
        //         return this.userLogin;
        //     }, (error) => {
        //
        //         console.log(error);
        //         this.error = error ;
        //
        //     });
    }

}
