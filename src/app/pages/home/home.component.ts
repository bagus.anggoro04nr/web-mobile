import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ParseJson} from '@src/app/_config/_libs/parse.json';
import { Injectable } from '@angular/core';
import {TransferData} from '@src/app/_config/_libs/transfer.data';
import {Menu} from '@src/app/_config/_models/menu';

@Injectable({ providedIn: 'root' })
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title = 'web-mobiles';
  returnUrl: string;
  dataMenu: any;
  valueFromJson: any;
  jsonValue: [] = [];
  constructor(public route: ActivatedRoute,
              public router: Router,
              public parseJson: ParseJson,
              public transferData: TransferData,
              public menuNya: Menu) {

  }

  ngOnInit() {

  }

  get menuData() {

      if (this.transferData.menuData != null) {

        this.valueFromJson = this.transferData.menuData;
        this.dataMenu = this.parseJson.dataParse(this.valueFromJson);
        this.menuNya.dataForMenu(this.dataMenu);
        return this.menuNya.htmlMenu;
      }
  }

  profile() {
    this.returnUrl = 'profile';
    this.router.navigate(['profile']);
  }

  email() {

  }

  absence() {

  }



}
