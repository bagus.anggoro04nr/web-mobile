import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from '@nativescript/angular';


import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/pages/home/home.component';
import { LoginComponent } from '@src/app/pages/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import { ProfilesComponent } from '@src/app/pages/profiles/profiles.component';
import { NavbarComponent } from '@src/app/shared/navbar/navbar.component';
import { ContentLayoutComponent } from '@src/app/shared/layout/content/content-layout.component';
import {TranslateModule} from '@ngx-translate/core';
import { MenuLayoutComponent } from '@src/app/shared/menu/menu-layout/menu-layout.component';


// Uncomment and add to NgModule imports if you need to use two-way binding and/or HTTP wrapper
// import { NativeScriptFormsModule, NativeScriptHttpClientModule } from '@nativescript/angular';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProfilesComponent,
    NavbarComponent,
    ContentLayoutComponent,
    MenuLayoutComponent,
  ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        FormsModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatDividerModule,
        ReactiveFormsModule,
        TranslateModule,

    ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
