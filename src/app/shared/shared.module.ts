import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from '@src/app/shared/navbar/navbar.component';
import { ContentLayoutComponent } from '@src/app/shared/layout/content/content-layout.component';



@NgModule({
    exports: [
       ContentLayoutComponent,
        NavbarComponent,

    ],
    imports: [
        RouterModule,
    ],
    declarations: [
        ContentLayoutComponent,
        NavbarComponent,

    ]
})
export class SharedModule { }
