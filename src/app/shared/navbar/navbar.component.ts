import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {TransferData} from '@src/app/_config/_libs/transfer.data';
import { AppComponent } from '@src/app/app.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  dataLogin: any;
  constructor(public router: Router, public route: ActivatedRoute, public transferData: TransferData,
              private appComponent: AppComponent) {

  }

  ngOnInit() {


  }

  get dataUser() {
    if (this.transferData.loginData != null) {

      const valueFromJson = this.transferData.loginData;
      this.dataLogin = JSON.parse(valueFromJson);

    }
    return this.dataLogin;
  }

    logout() {

        this.appComponent.logout();
    }

}
