import { Injectable, } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ParseJson {
    jsonData: Object[] = [];
    result: any;

    dataParse(dataJson) {
        // console.log(dataJson);
        this.result = JSON.parse(dataJson);
        // console.log(this.result);
        // tslint:disable-next-line:forin
       for (let i = 0 ; i < this.result.length; i++) {
            this.jsonData[i] = JSON.parse(this.result[i]);

        }
        // console.log(this.jsonData);
       return this.jsonData;

    }
}
