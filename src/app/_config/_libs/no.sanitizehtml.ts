import { Injectable, } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Injectable({ providedIn: 'root' })
export class NoSanitizehtml {
    constructor(private domSanitizer: DomSanitizer) {

    }
    isSanitize(html): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(html);
    }

}