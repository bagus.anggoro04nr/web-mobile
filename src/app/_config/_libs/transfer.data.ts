import { Injectable, } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TransferData {

    dataLogin(loginData) {
        if (typeof loginData === 'string') {
            localStorage.setItem('currentUser', loginData);

        }

    }

    dataMenu(menuData) {
        if (typeof menuData === 'string') {
            localStorage.setItem('menuUser', menuData);

        }

    }

    get loginData() {
        return localStorage.getItem('currentUser');
    }

    get menuData() {
        return localStorage.getItem('menuUser');
    }
}