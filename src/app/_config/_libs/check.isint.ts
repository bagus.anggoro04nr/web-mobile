import { Injectable, } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CheckIsint {

    isInt(value) {
        // tslint:disable-next-line:radix
        if ( parseInt(value) === 1) { return 1; } else { return 0; }
    }
}