/* tslint:disable */
import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable } from 'rxjs';
import { Users } from '../_models/users';
import { environment } from '../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<Users>;
    public currentUser: Observable<Users>;

    constructor(private http: HttpClient) {

        this.currentUserSubject = new BehaviorSubject<Users>(JSON.parse(JSON.stringify(localStorage.getItem('currentUser'))));
        this.currentUser = this.currentUserSubject.asObservable();
    }


    get currentUserValue(): Users {
        
        return this.currentUserSubject.value;
    }

    login(credentials) {

        return new Promise((resolve, reject) =>{

         this.http.post<any>(`${environment.apiUrl}/logins`, JSON.stringify(credentials) )
             .subscribe(response => {
                 resolve(JSON.parse(response));
                 // console.log(JSON.parse(response));
                 let user = JSON.parse(response);
                 this.currentUserSubject.next(user);
                 // return the modified data:
                 return JSON.parse(response); // kind of useless
             }, err => {
                 reject(JSON.parse(err));
                 console.log(JSON.parse(err));
             })
         ;
        });
    }

    menu(credentials) {

        return new Promise((resolve, reject) => {

            this.http.post<any>(`${environment.apiUrl}/menu`, JSON.stringify(credentials))
                .subscribe(resp => {
                    resolve(resp);
                    // console.log(resp);
                    return resp; // kind of useless
                }, err => {
                    reject(err);
                    console.log(err);
                })
            ;
        });
    }
        menunavbar(credentials) {

            return new Promise((resolve, reject) =>{

                this.http.post<any>(`${environment.apiUrl}/navbar`, JSON.stringify(credentials) )
                    .subscribe(respnavbar => {
                        resolve(JSON.parse(respnavbar));
                        // console.log(JSON.parse(respnavbar));

                        return JSON.parse(respnavbar); // kind of useless
                    }, err => {
                        reject(err);
                        console.log(err);
                    })
                ;
            });

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('menuUser');
        this.currentUserSubject.next(null);
    }
}
