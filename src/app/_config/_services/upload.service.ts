import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class UploadService {

    constructor(private http: HttpClient) {}

    upload(credentials) {


        return new Promise((resolve, reject) => {

            this.http.post<any>(`${environment.apiUrl}/uploads`, JSON.stringify(credentials))
                .subscribe(res =>{
                    resolve(res);
                    return res;
                }, (err) =>{
                    reject(err);
                });

        });
    }
}
