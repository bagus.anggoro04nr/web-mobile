import { Injectable } from '@angular/core';
import {Users} from '../_models/users';


@Injectable({ providedIn: 'root' })
export class UserService {

    constructor(public users: Users) {
        this.users = JSON.parse(localStorage.getItem('currentUser'));
    }
    get dataUser() {

        return this.users;
    }

}
