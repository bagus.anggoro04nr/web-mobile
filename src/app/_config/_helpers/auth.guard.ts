import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AuthenticationService } from '../_services/authentication.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      	
      	const currentUser = this.authenticationService.currentUserValue;
        
       
        if (currentUser === null) {
            
            // role not authorised so redirect to home page
            this.router.navigate(['']);
            return false;
            
        }else return true;
    }
}
