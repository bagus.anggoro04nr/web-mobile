import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '@src/app/pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfilesComponent } from './pages/profiles/profiles.component';
import { AuthGuard } from './_config/_helpers/auth.guard';
import { NavbarComponent } from '@src/app/shared/navbar/navbar.component';

export const routes: Routes = [

    {
        path: '',
        component: LoginComponent,

        pathMatch: 'full',
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
        pathMatch: 'full'

    },
    {  path: '' , component: NavbarComponent, canActivate: [AuthGuard],  pathMatch: 'full'},
    {
        path: 'profile',
        component: ProfilesComponent,
        pathMatch: 'full',
    },
    // otherwise redirect to home
    { path: '**', redirectTo: 'home', pathMatch: 'full' },
    { path: '#', redirectTo: 'home', pathMatch: 'full' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
