import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './_config/_services/authentication.service';
import { Users } from './_config/_models/users';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentUser: Users;
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
      public router: Router,
      public authenticationService: AuthenticationService,
      changeDetectorRef: ChangeDetectorRef,
      media: MediaMatcher
  ) {

    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

  }

  get isUser() {
        return this.currentUser;
    }

  logout() {

    this.authenticationService.logout();
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.router.navigate(['']);
  }

}
